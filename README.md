# add_method
Adds a method to the Object's prototype correctly.

[![Build Status](https://travis-ci.org/BebeSparkelSparkel/add_method.svg?branch=master)](https://travis-ci.org/BebeSparkelSparkel/add_method)
